#include <msp430.h>

#define CLK_RATE			10000000
#define true				1
#define false				0
#define LED1				BIT0

#define ACLK_SRC_VLO		2 << 4
#define ACLK_RATE			12000

#define T0_STOP_VAL			12000
#define T0_TACTL_INIT		TASSEL_1 | ID_0
#define T0_ENABLE_TIMER		MC_1 | TAIE

typedef unsigned int bool;

void setup_clock(void);
void setup_direction(void);
void set_led1_state(bool state);
void setup_timer(void);
void disable_timer(void);
void enable_timer(void);

bool led1_state = false;

/*
 * main.c
 */
int main(void) {
    WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer
//    P1SEL.0 and P1SEL2.0 should be 0, the default
//    P1IE.0 should be 0 to disable interrupt, it is 0 by default
//	  P1DIR.0 should be 1, by default it is 0
    IFG1 = 0;
    setup_direction();
    setup_clock();
    setup_timer();
    __enable_interrupt();
    enable_timer();

    while (true)
    	__low_power_mode_3();

//	return 0;
}


void setup_clock(void) {
	// setup MCLK
    BCSCTL1 = CALBC1_8MHZ;                    // Set range
    // ACLK set to VLO, divider is 1
    BCSCTL3 |= ACLK_SRC_VLO;
    DCOCTL = CALDCO_8MHZ;
}

void setup_direction(void) {
    P1DIR |= LED1;
}

void setup_timer(void) {
	TACTL = T0_TACTL_INIT;
	TACCR0 = T0_STOP_VAL;
	TACCTL0 = 0;
	TACCTL1 = 0;
	TACCTL2 = 0;
}

/**
 * LED1 is connected to P1.0
 */
#pragma FUNC_ALWAYS_INLINE (set_led1_state);
inline void set_led1_state(bool state) {
	P1OUT = (P1OUT & (~LED1)) | state;
}

#pragma FUNC_ALWAYS_INLINE (toggle_led1);
inline void toggle_led1(void) {
	P1OUT ^= LED1;
}


#pragma FUNC_ALWAYS_INLINE (disable_timer);
inline void disable_timer(void) {
	TACTL = T0_TACTL_INIT;
}

#pragma FUNC_ALWAYS_INLINE (enable_timer);
inline void enable_timer(void) {
	TACTL |= T0_ENABLE_TIMER;
}

#pragma FUNC_ALWAYS_INLINE (clear_timer);
inline void clear_timer(void) {
	TAR = 0;
}

#pragma vector=TIMER0_A1_VECTOR
__interrupt void Timer_A (void) {
	__disable_interrupt();
	disable_timer();
	clear_timer();
	toggle_led1();
	enable_timer();
	__enable_interrupt();
}
